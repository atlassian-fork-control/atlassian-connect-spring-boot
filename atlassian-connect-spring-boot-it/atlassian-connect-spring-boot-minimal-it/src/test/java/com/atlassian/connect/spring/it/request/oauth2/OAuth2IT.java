package com.atlassian.connect.spring.it.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.internal.request.oauth2.OAuth2RestTemplateFactory;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.BASE_URL;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.noAuthorizationHeader;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.onlyOneAuthorizationHeader;
import static com.atlassian.connect.spring.it.util.ConnectRequestMatchers.userAgentHeader;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OAuth2IT extends BaseApplicationIT {

    @Autowired
    private OAuth2RestTemplateFactory oAuth2RestTemplateFactory;

    @Autowired
    private AtlassianHostRestClients restClients;

    @Value("${atlassian.connect.client-version}")
    private String atlassianConnectClientVersion;

    private MockRestServiceServer mockProduct;
    private MockRestServiceServer mockAuthorizationServer;

    private static final String ACCESS_TOKEN = "Access-Token";
    private static final URI PRODUCTION_AUTHORIZATION_SERVER_URL = URI.create("https://oauth-2-authorization-server.services.atlassian.com");

    @Test
    public void shouldObtainThenReuseAccessTokenFromAuthzServer() {
        setJwtAuthenticatedPrincipal(createAndSaveHost(hostRepository), "bob");
        setUpMockAuthorizationServer();
        setUpMockHostProduct();
        expectAccessTokenRequestAndUse("/api1", BASE_URL + "/api1");
        setUpMockHostProduct();
        expectAccessTokenReuse("/api2", BASE_URL + "/api2");
    }

    private void expectAccessTokenRequestAndUse(String requestUrl, String expectedRequestUrl) {
        mockAuthorizationServer.expect(requestTo(PRODUCTION_AUTHORIZATION_SERVER_URL + "/oauth2/token"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(noAuthorizationHeader())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(content().string(stringContainsInOrder(Arrays.asList(
                        "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer",
                        "assertion="
                ))))
                .andRespond(withSuccess(("{" +
                        "`expires_in`: 3600," +
                        "`access_token`: `" + ACCESS_TOKEN + "`," +
                        "`token_type`: `bearer`" +
                        "}").replaceAll("`", "\""), APPLICATION_JSON_UTF8));
        mockProduct.expect(requestTo(expectedRequestUrl))
                .andExpect(authorizationHeaderWithOAuth2AccessToken())
                .andExpect(onlyOneAuthorizationHeader())
                .andExpect(userAgentHeader(atlassianConnectClientVersion))
                .andRespond(withSuccess());

        restClients.authenticatedAsHostActor().getForObject(requestUrl, Void.class);

        mockAuthorizationServer.verify();
        mockProduct.verify();
    }

    private RequestMatcher authorizationHeaderWithOAuth2AccessToken() {
        return header(HttpHeaders.AUTHORIZATION,
                allOf(equalToIgnoringCase("Bearer " + ACCESS_TOKEN), endsWith(ACCESS_TOKEN)));
    }

    private void expectAccessTokenReuse(String requestUrl, String expectedRequestUrl) {
        mockProduct.expect(requestTo(expectedRequestUrl))
                .andExpect(authorizationHeaderWithOAuth2AccessToken())
                .andExpect(onlyOneAuthorizationHeader())
                .andRespond(withSuccess());

        restClients.authenticatedAsHostActor().getForObject(requestUrl, Void.class);

        mockAuthorizationServer.verify();
        mockProduct.verify();
    }

    private void setUpMockAuthorizationServer() {
        RestTemplate restTemplate = oAuth2RestTemplateFactory.getAccessTokenProviderRestTemplate();
        mockAuthorizationServer = MockRestServiceServer.createServer(restTemplate);
    }

    private void setUpMockHostProduct() {
        mockProduct = MockRestServiceServer.createServer(restClients.authenticatedAsHostActor());
    }
}
