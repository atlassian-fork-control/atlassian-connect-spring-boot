package com.atlassian.connect.spring.it.util;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.read.ListAppender;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LogbackCapture implements TestRule {

    private final ListAppender<ILoggingEvent> listAppender = new ListAppender<>();

    @Override
    public Statement apply(Statement statement, Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
                context.reset();

                Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
                logger.addAppender(listAppender);
                listAppender.start();

                try {
                    statement.evaluate();
                } finally {
                    listAppender.stop();
                    context.reset();
                    new ContextInitializer(context).autoConfig();
                }
            }
        };
    }

    public List<ILoggingEvent> getEvents() {
        return this.listAppender.list;
    }
}
