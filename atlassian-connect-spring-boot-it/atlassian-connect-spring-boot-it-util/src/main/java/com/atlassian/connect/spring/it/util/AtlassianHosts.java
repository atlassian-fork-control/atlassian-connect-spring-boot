package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;

public final class AtlassianHosts {

    public static final String CLIENT_KEY = "some-host";
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgHt+Liuc+8AYy41mzpoLUkVZ/n9gsOaVRiDUXHnUkQOlf2LHGw1PdpO26Rawf7HBvA5XIxEs6aw2J/62DPj86P7sT7bOIfh6Z6cLX+E6liTWPH44lNR5CXuI++GXoZbm44Q80uFwltEZZScHtIzPeiO4ctR9o7mRZ1E3ZfctF8Qk1MlNRFbumZSe6JQWCC63LyWO5ABs2ATYi7qcEwI9k1nqmjjUEe/W77erBWmhusnLNwdAQdcCtD1IdwZk3QMuozqPpMeSW3Je5Imrj0pBgw/uipWnSQwKO2hOLjKdw9y7cZOvSW7FCvbOsIam7paT+pbW1pkaFhU8yv0NJwsz2QIDAQAB";
    public static final String PRODUCT_TYPE = "some-product";

    public static final String SHARED_SECRET =       "11111111111111111111111111111111"; // must be 32 characters long
    public static final String OTHER_SHARED_SECRET = "22222222222222222222222222222222";

    public static final String BASE_URL = "http://example.com/product";

    private AtlassianHosts() {}

    public static AtlassianHost createAndSaveHost(AtlassianHostRepository hostRepository) {
        AtlassianHost host = new AtlassianHostBuilder().build();
        hostRepository.save(host);
        return host;
    }
}
