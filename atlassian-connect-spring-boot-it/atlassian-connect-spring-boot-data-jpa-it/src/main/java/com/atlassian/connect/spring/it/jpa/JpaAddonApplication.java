package com.atlassian.connect.spring.it.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaAddonApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(JpaAddonApplication.class).run(args);
    }
}
