package specs;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.owner.PlanOwner;
import com.atlassian.bamboo.specs.api.builders.pbc.ContainerSize;
import com.atlassian.bamboo.specs.api.builders.pbc.PerBuildContainerForJob;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.util.BambooServer;

import java.time.Duration;

@BambooSpec
public class SourceClearPlan {

    public Plan plan() {
        Project project = new Project().key(new BambooKey("ACA"));
        return new Plan(project, "atlassian-connect-spring-boot SourceClear", new BambooKey("ACSBSC"))
                .pluginConfigurations(new PlanOwner("epehrson"))
                .linkedRepositories("atlassian-connect-spring-boot")
                .triggers(new RepositoryPollingTrigger()
                        .triggeringRepositoriesType(RepositoryBasedTrigger.TriggeringRepositoriesType.SELECTED)
                        .selectedTriggeringRepositories(new VcsRepositoryIdentifier().name("atlassian-connect-spring-boot"))
                        .withPollingPeriod(Duration.ofSeconds(60)))
                .planBranchManagement(new PlanBranchManagement()
                        .createForVcsBranch()
                        .delete(new BranchCleanup()
                                .whenRemovedFromRepositoryAfterDays(7)
                                .whenInactiveInRepositoryAfterDays(30)))
                .stages(new Stage("Default Stage")
                        .jobs(new Job("SourceClear",
                                new BambooKey("SRC"))
                                .description("Scan for security vulnerabilities in 3rd-party libraries.")
                                .pluginConfigurations(new PerBuildContainerForJob()
                                        .enabled(true)
                                        .image("docker.atl-paas.net/buildeng/agent-baseagent")
                                        .size(ContainerSize.REGULAR))
                                .tasks(new VcsCheckoutTask()
                                                .description("Checkout latest code")
                                                .checkoutItems(new CheckoutItem()
                                                        .repository(new VcsRepositoryIdentifier()
                                                                .name("atlassian-connect-spring-boot"))),
                                        new ScriptTask()
                                                .description("SourceClear Scan")
                                                .environmentVariables("SRCCLR_API_TOKEN=${bamboo.CONNECT_SRCCLR_API_TOKEN_PASSWORD}")
                                                .inlineBody("curl -sSL https://download.sourceclear.com/ci.sh | sh"))));
    }

    public static void main(String... args) {
        BambooServer bambooServer = new BambooServer("https://ecosystem-bamboo.internal.atlassian.com");
        bambooServer.publish(new SourceClearPlan().plan());
    }
}
