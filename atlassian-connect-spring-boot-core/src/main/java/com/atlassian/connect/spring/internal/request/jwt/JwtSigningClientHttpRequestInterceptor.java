package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.util.Optional;
import java.util.function.Function;

/**
 * A {@link ClientHttpRequestInterceptor} that signs requests to Atlassian hosts with JSON Web Tokens.
 */
public class JwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private JwtGenerator jwtGenerator;

    private Function<HttpRequest, Optional<AtlassianHost>> hostSupplier;

    public JwtSigningClientHttpRequestInterceptor(JwtGenerator jwtGenerator,
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion,
            AtlassianHostUriResolver hostUriResolver,
            AtlassianConnectSecurityContextHelper securityContextHelper) {
        super(atlassianConnectClientVersion);
        this.jwtGenerator = jwtGenerator;
        this.hostSupplier = request -> getHostFromContext(hostUriResolver, securityContextHelper, request);
    }

    public JwtSigningClientHttpRequestInterceptor(JwtGenerator jwtGenerator,
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion,
            AtlassianHost host) {
        super(atlassianConnectClientVersion);
        this.jwtGenerator = jwtGenerator;
        this.hostSupplier = request -> Optional.of(host);
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        return hostSupplier.apply(request);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        String jwt = jwtGenerator.createJwtToken(request.getMethod(), request.getURI(), host);
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", jwt));
        return request;
    }

    private Optional<AtlassianHost> getHostFromContext(AtlassianHostUriResolver hostUriResolver, AtlassianConnectSecurityContextHelper securityContextHelper, HttpRequest request) {
        Optional<AtlassianHost> optionalHost = securityContextHelper.getHostFromSecurityContext()
                .filter((host) -> AtlassianHostUriResolver.isRequestToHost(request.getURI(), host));
        if (!optionalHost.isPresent()) {
            optionalHost = hostUriResolver.getHostFromRequestUrl(request.getURI());
        }
        return optionalHost;
    }
}
